require "functions"

Languages={{"en","fr"},{"fr","en"}}

RepoDir = RepoDir or os.getenv('PWD')
Host = "127.0.0.9"
Port = 80
ServerAdmin = "webmaster@127.0.0.9"
ServerName = "127.0.0.9"
ServerAlias = "127.0.0.9"
ProxyModule = true -- set to false if you would like to use rewrite rules
ProxyURL = "http://iridl.ldeo.columbia.edu"
MacSetup = true

if ProxyModule then
ProxyRules = [[
   ProxyPass /SOURCES ]]..ProxyURL..[[/SOURCES nocanon
   ProxyPass /expert ]]..ProxyURL..[[/expert nocanon
   ProxyPass /ds: ]]..ProxyURL..[[/ds: nocanon
   ProxyPass /home ]]..ProxyURL..[[/home nocanon
   ProxyPass /dllib ]]..ProxyURL..[[/dllib nocanon
   ProxyPass /openrdf-sesame ]]..ProxyURL..[[/openrdf-sesame nocanon
]]
else
ProxyRules = [[
   RewriteRule ^/SOURCES/(.*) ]]..ProxyURL..[[/SOURCES/$1
   RewriteRule ^/expert/(.*) ]]..ProxyURL..[[/expert/$1
   RewriteRule ^/ds:/(.*) ]]..ProxyURL..[[/ds:/$1
   RewriteRule ^/home/(.*) ]]..ProxyURL..[[/home/$1
   RewriteRule ^/dllib/(.*) ]]..ProxyURL..[[/dllib/$1
   RewriteRule ^/openrdf-sesame/(.*) ]]..ProxyURL..[[/openrdf-sesame/$1
]]
end

--[[ This starts the section of parameters for the Tutorials.--]]

--[[Parameters for Tutoria/Datasets/Location.--]]

--[[ Data by Source --]]
LocationText1 = "ENACTS"
LocationText2 = "Niger"
LocationText3 = "ENACTS"
LocationText4 = "ENACTS monthly temperature"
LocationText5 = "Select the <i>ENACTS</i> link.<br />Select the <i>ALL</i> link.<br />Select the <i>monthly</i> link.<br />Select the <i>temperature</i> link."
LocationText8 = "gridded"
LocationHref1 = "/SOURCES/.Niger/.ENACTS/.ALL/.monthly/.temperature/"
--[[ Data By Searching --]]
LocationText9 = "Niger ENACTS daily temperature tmax"
LocationHref2 = "/SOURCES/.Niger/.ENACTS/.daily/.temperature/.tmax/"
LocationText10 = "temperature"
LocationHref3 = "/SOURCES/.Niger/.ENACTS/.daily/.temperature/"
LocationText11 = "Niger ENACTS daily temperature"

--[[Parameters for Tutoria/Datasets/Structure.--]]

StructureText1 = "Niger ENACTS daily temperature"
StructureHref1 = "/SOURCES/.Niger/.ENACTS/.daily/.temperature/"
StructureText2 = "temperature"
--[[NB: /localconfig/figures/StructureFig2.png need be replaced.--]]
StructureText3 = "ENACTS"
StructureText4 = "ENACTS daily"
StructureText5 = "daily"
StructureText6 = "tmin"
StructureText7 = "longitude and latitude"
StructureText8 = "days since 1961-01-01 12:00:00"
StructureText9 = "1 Jan 1961"
StructureText10 = "17 Jun 2020"
StructureText11 = "21718"
StructureHref2 = "/SOURCES/.NOAA/.NCDC/.ERSST/.version5/"
StructureText12 = "SOURCES NOAA NCDC ERSST version5"

--[[Parameters for Tutorial/Selection/Station_Regions--]]

--[[ Gridded Data --]]
Stations_RegionsHref1 = "/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/"
Stations_RegionsText1 = "Niger ENACTS monthly climatologies"
Stations_RegionsText2 = "6°E, 15°N"
Stations_RegionsText3 = "rfe merged"
Stations_RegionsText4 = "Monthly Rainfall Climatology 1981-2019 average"
Stations_RegionsHref2 = "/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/rfe_merged/"
Stations_RegionsText5 = "6E"
Stations_RegionsText6 = "15N"
Stations_RegionsHref3 = "/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/rfe_merged/"
Stations_RegionsText7 = "in the middle of Niger"
Stations_RegionsText8 = "climatologies"
Stations_RegionsHref4 = "/expert/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/"
Stations_RegionsText9 = ".rfe_merged"
Stations_RegionsHref5 = "/expert/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/.rfe_merged_chirps/"
Stations_RegionsText10 = "103°-107°E, 21°-23°N"
Stations_RegionsText11 = "23N"
Stations_RegionsText12 = "21N"
Stations_RegionsText13 = "103E"
Stations_RegionsText14 = "107E"
Stations_RegionsText15 = "a Northern part of Niger"
Stations_RegionsHref6 = "/"
Stations_RegionsText16 = "23"
Stations_RegionsText17 = "21"
Stations_RegionsText18 = "103"
Stations_RegionsText19 = "107"

--[[Parameters for Tutorial/Selection/Data_Variables--]]

--[[ Single --]]
Data_VariablesText1 = "CDAS-1 monthly intrinsic"
Data_VariablesHref1 = "/SOURCES/.NOAA/.NCEP-NCAR/.CDAS-1/.MONTHLY/.Intrinsic/"
Data_VariablesText2 = "Pressure level zonal wind"
Data_VariablesText3 = "Select the <i>PressureLevel</i> link under the Datasets and variables heading.<br />Select the <i>zonal wind</i> link under the Datasets and variables heading."
Data_VariablesText4 = "PressureLevel zonal wind"
Data_VariablesHref2 = ".PressureLevel/.u/"
Data_VariablesText6 = ".PressureLevel .u"
Data_VariablesHref3 = "/expert"
--[[ Multiple --]]
Data_VariablesText7 = "Niger monthly"
Data_VariablesHref4 = "/SOURCES/.Niger/.ENACTS/.monthly/"
Data_VariablesText8 = "monthly climatological rainfall"
Data_VariablesText9 = "monthly observed rainfall"
Data_VariablesHref5 = ".climatologies/.rfe_merged_chirps/"
Data_VariablesHref6 = ".rainfall/.rfe_merged"
Data_VariablesText10 = "SOURCES .Niger .ENACTS .monthly .climatologies .rfe_merged_chirps"

--[[Parameters for Tutorial/Selection/Time_Period--]]

--[[ Continuous --]]
Time_PeriodHref1 = "/SOURCES/.Niger/.ENACTS/.dekadal/.rainfall/"
Time_PeriodText1 = "dekadal rainfall"
Time_PeriodText2 = "November 17, 1982 - December 29, 2001"
Time_PeriodText3 = "Merged Station-Satellite Rainfall"
Time_PeriodHref2 = ".rfe_merged/"
Time_PeriodText4 = "17 Nov 1982 to 29 Dec 2001"
Time_PeriodHref3 = "17+Nov+1982+to+29+Dec+2001"
Time_PeriodText5 = "Note that the limits of the time grid in the Data Selection box match the closest Wednesday to your selected time limits as dekad data are encompassing about 10 days."
Time_PeriodHref4 = "%2817%20Nov%201982%29%2829%20Dec%202001%29RANGE/"
Time_PeriodText6 = ".rfe_merged"
Time_PeriodText7 = "T (17 Nov 1982) (29 Dec 2001) RANGE"
--[[ Discontinuous --]]
Time_PeriodHref5 = "/SOURCES/.Niger/.ENACTS/.daily/.temperature/"
Time_PeriodText8 = "Maximum Daily Temperature reanalysis merged with station"
Time_PeriodHref6 = ".tmax/"

--[[Parameters for Tutorial/MVD/Manipulation--]]

--[[ Arithmetic --]]
MVDparameterText1 = "C"
MVDparameterText2 = "a gridbox (6°E, 15°N) in Niger"
MVDparameterHref1 = "/SOURCES/.Niger/.ENACTS/.daily/.temperature/"
MVDparameterText3 = "Niger ENACTS daily temperature"
MVDparameterText4 = ""
MVDparameterHref2 = "X/6/VALUE/Y/15/VALUE/"
MVDparameterHref3 = ".tmin/"
MVDparameterText5 = "precipitation"
MVDparameterHref4 = "/SOURCES/.Niger/.ENACTS/.monthly/"
MVDparameterText6 = "Niger ENACTS monthly"
MVDparameterText7b = " (months since 01-Jan) periodic Jan to Dec by 1. N= 12 pts"
MVDparameterText7 = " (months since 1960-01-01) ordered (Jan 1981) to (Dec 2019) by 1.0 N= 468 pts"
MVDparameterText8 = "Monthly Rainfall"
MVDparameterHref5 = ".climatologies/.rfe_merged"
MVDparameterHref6 = ".rainfall/.rfe_merged/yearly-anomalies/"
MVDparameterText9 = "6°E, 15°N"
MVDparameterText10 = "15N"
MVDparameterText11 = "6E"
MVDparameterHref7 = ".rainfall/.rfe_merged"
MVDparameterText12 = "monthly precipitation"
MVDparameterText13 = "mm"
MVDparameterText14 = "cm"
MVDparameterText15 = "0.1"
MVDparameterHref8 = ".rainfall/.rfe_merged/"
MVDparameterText16 = "mm"
MVDparameterText17 = "m"
MVDparameterText18 = "1000."
MVDparameterHref4b = "/SOURCES/.Niger/.ENACTS/.monthly/"
MVDparameterText6b = "Niger ENACTS monthly"
MVDparameterHref7b = ".rainfall/.rfe_merged/"
--[[ Limits --]]
MVDparameterText19 = "10"
MVDparameterText20 = "Celsius"
MVDparameterText21 = "10."
MVDparameterText22 = "gridbox (6°E, 15°N) in Niger"
MVDparameterHref9 = "X/6/VALUE/Y/15/VALUE/"
MVDparameterText23 = "30."
MVDparameterHref10 = ".tmax/"
MVDparameterText24 = "gridbox (6°E, 15°N) in Niger"
MVDparameterHref11 = "X/6/VALUE/Y/15/VALUE/"
MVDparameterText25 = "February 23rd"
MVDparameterText26 = "minimum temperature"
MVDparameterText27 = "10"
MVDparameterText28 = "˚C"
MVDparameterHref12 = ".tmin/"
MVDparameterText29 = "˚C"
MVDparameterText30 = "10."
MVDparameterText31 = "gridbox (6°E, 15°N) in Niger"
MVDparameterHref13 = "X/6/VALUE/Y/15/VALUE/"
--[[ Averages --]]
MVDparameterText32 = "Niger"
MVDparameterHref14 = ""
MVDparameterText41 = "minimum temperature"
MVDparameterHref3b = ".tmin/"
MVDparameterText33 = "Jan 1, 1961 - Oct 26, 2020"
MVDparameterText34 = "Jan 8, 1961 - Oct 19, 2020"
MVDparameterText35 = "gridbox (6°E, 15°N) in Niger"
MVDparameterHref15 = "X/6/VALUE/Y/15/VALUE/"
--[[ Statistical --]]
MVDparameterText36 = "a gridbox (6°E, 15°N) in Niger"
MVDparameterText8c = "SST anomalies"
MVDparameterText37 = "Niger ENACTS monthly precipitation"
MVDparameterHref16 = "/SOURCES/.Niger/.ENACTS/.monthly/"
MVDparameterText38 = "the gridbox (6°E, 15°N) in Niger"
MVDparameterHref17 = "X/6/VALUE/Y/15/VALUE/%5BX/Y%5Daverage/"
MVDparameterHref18 = ".rainfall/.rfe_merged/"
MVDparameterText39 = "SOURCES .NOAA .NCDC .ERSST .version5 .anom"
MVDparameterHref4c = "/SOURCES/.NOAA/.NCDC/.ERSST/.version5/"
MVDparameterHref6c = ".anom/"
MVDparameterText40 = "Niger ENACTS monthly precipitation"
MVDparameterHref19 = ".rainfall/.rfe_merged/"

--[[Parameters for Tutorial/MVD/Visualization--]]

--[[ Color Map --]]
VisualizationHref1 = "/SOURCES/.NOAA/.NCDC/.ERSST/.version5/"
VisualizationText1 = "ERSST v5"
VisualizationHref2 = ".anom/"
--[[ Contour Map --]]
VisualizationText2 = "July"
VisualizationText3 = "Niger monthly climatology rainfall"
VisualizationText4 = ""
VisualizationText5 = "Niger monthly rainfall"
VisualizationHref3 = ".Niger/.ENACTS/.monthly/"
VisualizationText6 = ""
VisualizationHref4 = ".climatologies/.rfe_merged_chirps/T/%28Jul%29VALUES/"
VisualizationText8 = "150"
VisualizationText8b = "mm"
--[[ 1D plots --]]
VisualizationText9 = "rainfall rfe_merged"
VisualizationText10 = "6°E, 15°N"
VisualizationHref5 = ".rainfall/.rfe_merged/"
VisualizationHref6 = "Y/%2815N%29VALUES/X/%286E%29VALUES/"
VisualizationText11 = "precip_colors"
VisualizationHref7 = "/SOURCES/.Niger/.ENACTS/.monthly/.rainfall/.CHIRPS/.rfe_merged/X/6/VALUE/Y/15/VALUE/SOURCES/.Niger/.ENACTS/.monthly/.temperature/.tmax/X/6/VALUE/Y/15/VALUE/T/fig-/colorbars2/-fig/#expert"
VisualizationText12 = "monthly rainfall and the color of the bars indicates maximum temperature"
VisualizationHref8 = "/SOURCES/.Niger/.ENACTS/.monthly/.temperature/.tmax/T/(Feb 1982)/VALUE/SOURCES/.Niger/.ENACTS/.monthly/.temperature/.tmin/T/(Feb 1982)/VALUE/SOURCES/.Niger/.ENACTS/.monthly/.rainfall/.CHIRPS/.rfe_merged/T/(Feb 1982)/VALUE/fig-/scattercolor/-fig/#expert"
VisualizationText13 = "tmin and tmax to scatter the Feb 1982 data points while the color of each point represents rainfall"
--[[ Vectors plots --]]
--[[ Cross Section plots --]]
--[[ Animation --]]
VisualizationText14 = "monthly"
VisualizationHref9 = ".sst/"
--[[ Colorscale --]]
VisualizationText15 = "Niger temperature anomaly"
VisualizationText16 = "Niger temperature anomaly calculated here for you"
VisualizationHref10 = ".Niger/.ENACTS/.monthly/temperature/.tmax/climatologies/.tmax/sub/"
--[[ Land Mask --]]
--[[ MJO --]]


--[[Parameters for Tutorial/MVD/Download--]]
DownloadText1 = "Niger monthly rainfall climatologies"
DownloadHref1 = ".Niger/.ENACTS/.monthly/.climatologies/.rfe_merged_chirps/"
DownloadText2 = "rainfall climatology"
DownloadText3 = "2232000"
