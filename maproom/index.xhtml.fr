<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      xml:lang="fr"
      >

  <head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
    <title>Maproom Direction Nationale de la Météorologie</title>

    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
    <link rel="stylesheet" type="text/css" href="/maproom/niger.css" />

    <link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
    <meta property="maproom:Sort_Id" content="a01" />
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />

    <link class="carryLanguage" rel="home" href="/" title="Data Library" />
    <link rel="canonical" href="index.html" />
    <link class="carryLanguage" rel="home alternate" type="application/json" href="/localconfig/navmenu.json" />
    <link rel="shortcut icon" href="/localconfig/icons/iriwh128.png" />
    
    <link rel="term:icon" href="/SOURCES/.Niger/.ENACTS/.dekadal/.climatologies/%28.rfe_merged_chirps%29//var/parameter/dup/interp/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/||/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef/T/last/plotvalue+.gif" />

    <script type="text/javascript" src="/uicore/uicore.js"></script>
  </head>

  <body xml:lang="fr">
    <form name="pageform" id="pageform">
      <input class="carryLanguage" name="Set-Language" type="hidden" />
      <input class="titleLink itemImage" name="bbox" type="hidden" />
    </form>

    <div class="controlBar">
      <fieldset class="navitem">
        <legend>DNM</legend>
        <a rev="section" class="navlink carryup" href="/">Data Library - Climat</a>
      </fieldset>

      <fieldset class="navitem">
        <legend>Data Library</legend>
        <span class="navtext">Maproom</span>
      </fieldset>

      <!-- <fieldset class="navitem" id="chooseRegion">
        <legend>Region</legend>
      </fieldset> -->
    </div>

    <div>
      <div id="content" class="searchDescription">
        <h2 property="term:title">DNM Map Room</h2>
        <p align="left" property="term:description">
        Cette maproom est une collection de cartes et autre figures qui permettent de surveiller le climat et les aspects sociétaux actuels et dans un passé récent. Les cartes et figures peuvent être manipulées et sont liées aux données originales. Même si vous êtes principalement interessés par les données plutôt que par les cartes et figures, cette page vous permet de voir quelles données sont particulièrement utiles pour surveiller les conditions climatiques.</p>
      </div>

      <div class="rightcol tabbedentries" about="/maproom/" ></div>
    </div>

    <div class="optionsBar">
      <fieldset class="navitem" id="share"><legend>Partager</legend></fieldset>
    </div>

  </body>

 </html>
