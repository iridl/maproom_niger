<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="en">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <meta xml:lang="" property="maproom:Entry_Id" content="Climatology_Analysis" />
  <meta xml:lang="" property="maproom:Sort_Id" content="a1" />
  <title>Daily Precipitation Analysis</title>

  <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
  <link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
  <link rel="stylesheet" type="text/css" href="/maproom/niger.css" />

  <link class="share" rel="canonical" href="daily_precip.html" />
  <link class="carryLanguage" rel="home" href="http://iridl.ldeo.columbia.edu" />
  <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
  <link class="altLanguage" rel="alternate" hreflang="fr" href="daily_precip.html?Set-Language=fr" />

  <link rel="shortcut icon" href="/localconfig/icons/iriwh128.png" />
  <script type="text/javascript" src="/uicore/uicore.js"></script>
  <script type="text/javascript" src="/localconfig/ui.js"></script>
  <script type="text/javascript" src="/uicore/insertui.js"></script>
  <!-- <script type="text/javascript" src="/maproom/metmalawi.js"></script> -->

  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily"/>

  <link rel="term:icon" href="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/%281981%29//YearStart/parameter/%282019%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7Bdup/dataflag%5BT%5Dsum/dup/1.0/sub/div/sqrt/2/1/roll%5BT%5Drmsaover/mul/MEWSprcp_colors%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7BDATA/0/300/1/RANGESTEP%5BX/Y%5Ddistrib1D/dup/0.0%5Baprod%5Dintegrate/aprod/300.5/VALUE/aprod/removeGRID/div//name/%28pdf%29/def/DATA/null/null/RANGE/0.0%5Baprod%5Dintegrate//name/%28pdf%29/def/DATA/0/0.02/0.17/0.5/0.83/0.98/1.0001/VALUES/-1/mul/1/add/aprod//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28150%29//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%2830%29//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%2810%29//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%283%29//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%283%29//probExcThresh5/parameter/interp%7Dif/VALUE/%28percent%29/unitconvert//long_name/%28Probability%29/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap%7Dif//long_name/%28%0A/seasonalStat%20get_parameter%0A%28TotRain%29%20eq%0A%7B%20%28Total%20Rainfall%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWD%29%20eq%0A%7B%20%28Wet%20Days%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28RainInt%29%20eq%0A%7B%20%28Rainfall%20Intensity%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumDS%29%20eq%0A%7B%20%28Dry%20Spells%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWS%29%20eq%0A%7B%20%28Wet%20Spells%29%20%7Dif%0A%28%20:%20%29%0A/yearlyStat%20get_parameter%0A%28Mean%29%20eq%20%7B%20%28Average%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28StdDev%29%20eq%20%7B%20%28Standard%20Deviation%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28PoE%29%20eq%20%7B%20%28Probability%29%20%7Dif%0A%29/interp/3/array/astore/concat/def/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name/%28clim_var%29/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/||/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20for%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20to%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef//antialias+true+psdef+.gif" />


  <style>
    body[layers] #auxvar {
      display: none;
    }
    body[layers] #auxtopo {
      display:none;
    }
    body[layers~="clim_var"] #auxvar {
      display: inline;
    }
    body[layers~="bath"] #auxtopo {
      display: inline;
    }
    body[layers~="clim_var"] #auxtopo {
      display: none;
    }
    .seasonalStatTotRain #pET2 {
      display: none;
    }
    .seasonalStatTotRain #pET3 {
      display: none;
    }
    .seasonalStatTotRain #pET4 {
      display: none;
    }
    .seasonalStatTotRain #pET5 {
      display: none;
    }
    .seasonalStatNumWD #pET1 {
      display: none;
    }
    .seasonalStatNumWD #pET3 {
      display: none;
    }
    .seasonalStatNumWD #pET4 {
      display: none;
    }
    .seasonalStatNumWD #pET5 {
      display: none;
    }
    .seasonalStatRainInt #pET1 {
      display: none;
    }
    .seasonalStatRainInt #pET2 {
      display: none;
    }
    .seasonalStatRainInt #pET4 {
      display: none;
    }
    .seasonalStatRainInt #pET5 {
      display: none;
    }
    .seasonalStatNumDS #pET1 {
      display: none;
    }
    .seasonalStatNumDS #pET2 {
      display: none;
    }
    .seasonalStatNumDS #pET3 {
      display: none;
    }
    .seasonalStatNumDS #pET5 {
      display: none;
    }
    .seasonalStatNumWS #pET1 {
      display: none;
    }
    .seasonalStatNumWS #pET2 {
      display: none;
    }
    .seasonalStatNumWS #pET3 {
      display: none;
    }
    .seasonalStatNumWS #pET4 {
      display: none;
    }
    .seasonalStat #pET2 {
      display: none;
    }
    .seasonalStat #pET3 {
      display: none;
    }
    .seasonalStat #pET4 {
      display: none;
    }
    .seasonalStat #pET5 {
      display: none;
    }
    .seasonalStatPerDA #yearlyStat {
      display: none;
    }
    .yearlyStatMean #pET1 {
      display: none;
    }
    .yearlyStatStdDev #pET1 {
      display: none;
    }
    .yearlyStat #pET1 {
      display: none;
    }
    .yearlyStatMean #pET2 {
      display: none;
    }
    .yearlyStatStdDev #pET2 {
      display: none;
    }
    .yearlyStat #pET2 {
      display: none;
    }
    .yearlyStatMean #pET3 {
      display: none;
    }
    .yearlyStatStdDev #pET3 {
      display: none;
    }
    .yearlyStat #pET3 {
      display: none;
    }
    .yearlyStatMean #pET4 {
      display: none;
    }
    .yearlyStatStdDev #pET4 {
      display: none;
    }
    .yearlyStat #pET4 {
      display: none;
    }
    .yearlyStatMean #pET5 {
      display: none;
    }
    .yearlyStatStdDev #pET5 {
      display: none;
    }
    .yearlyStat #pET5 {
      display: none;
    }
  </style>
</head>

<body  xml:lang="en">
  <form name="pageform" id="pageform" class="carryLanguage carryup carry dlimg dlauximg share">
    <input name="Set-Language" type="hidden" class="carryup carryLanguage"/>
    <input class="carry dlimg dlauximg share dlimgloc dlimglocclick  admin" name="bbox" type="hidden" />
    <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
    <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
    <input class="dlimg dlauximg" name="plotaxislength" type="hidden" />

    <input class="dlimg dlauximg share dlimgts" name="DayStart" type="hidden" value="01" />
    <input class="carry dlimg dlauximg share dlimgts" name="seasonStart" type="hidden" value="Jul" />
    <input class="carry dlimg dlauximg share dlimgts" name="YearStart" type="hidden" value="1981" />

    <input class="dlimg dlauximg share dlimgts" name="DayEnd" type="hidden" value="31" />
    <input class="carry dlimg dlauximg share dlimgts" name="seasonEnd" type="hidden" value="Aug" />
    <input class="carry dlimg dlauximg share dlimgts" name="YearEnd" type="hidden" value="2019" />

    <input class="dlimg dlauximg share dlimgts" name="wetThreshold" type="hidden" value="1." />
    <input class="dlimg dlauximg share dlimgts" name="spellThreshold" type="hidden" value="5." />
    <input class="dlimg dlauximg share dlimgts pET ylyStat bodyClass" name="seasonalStat" type="hidden" value="TotRain" />
    <input class="dlimg dlauximg share ylyStat pET bodyClass" name="yearlyStat" type="hidden" value="Mean" />
    <input class="dlimg share pET" name="probExcThresh1" type="hidden" value="40." />
    <input class="dlimg share pET" name="probExcThresh2" type="hidden" value="30." />
    <input class="dlimg share pET" name="probExcThresh3" type="hidden" value="10." />
    <input class="dlimg share pET" name="probExcThresh4" type="hidden" value="3." />
    <input class="dlimg share pET" name="probExcThresh5" type="hidden" value="3." />

    <input class="carry pickarea share admin" name="resolution" type="hidden" value="0.05" />
    <input class="dlimg share bodyAttribute" name="layers" value="clim_var" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="bath" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Regions" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Districts" checked="checked" type="checkbox" />
  </form>

  <div class="controlBar">
    <fieldset class="navitem" id="toSectionList">
      <legend>Maproom</legend>
      <a rev="section" class="navlink carryup" href="/maproom/Climatology/">Climate</a>
    </fieldset>

    <fieldset class="navitem" id="chooseSection">
      <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"><span property="term:label">Climate Analysis</span></legend>
    </fieldset>

    <!-- <fieldset class="navitem">
      <legend>Region</legend>
      <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/metmalawiRegions.json"></a>
      <select class="RegionMenu" name="bbox">
        <option value="">Malawi</option>
        <optgroup class="template" label="Region">
          <option class="irigaz:hasPart irigaz:id@value term:label"></option>
        </optgroup>
      </select>
    </fieldset> -->

    <fieldset class="navitem">
      <legend>Seasons (1981 to 2019)</legend>
      <select class="pageformcopy" name="seasonStart">
        <option>Jan</option> <option>Feb</option> <option>Mar</option> <option>Apr</option><option>May</option><option>Jun</option><option>Jul</option><option>Aug</option><option>Sep</option><option>Oct</option><option>Nov</option><option>Dec</option>
      </select>
      <input class="pageformcopy" name="DayStart" type="text" value="01" size="2" maxlength="2"/>
      <input class="pageformcopy" name="YearStart" type="text" value="1981" size="4" maxlength="4"/> to
      <select class="pageformcopy" name="seasonEnd">
        <option>Jan</option><option>Feb</option><option>Mar</option><option>Apr</option><option>May</option><option>Jun</option><option>Jul</option><option>Aug</option><option>Sep</option><option>Oct</option><option>Nov</option><option>Dec</option>
      </select>
      <input class="pageformcopy" name="DayEnd" type="text" value="31" size="2" maxlength="2"/>
      <input class="pageformcopy" name="YearEnd" type="text" value="2019" size="4" maxlength="4"/>
    </fieldset>

    <fieldset class="navitem">
      <legend>Seasonal daily statistics</legend>
      <select class="pageformcopy" name="seasonalStat">
        <option value="TotRain">Total Rainfall</option>
        <option value="NumWD">Number of Wet Days</option>
        <option value="RainInt">Rainfall Intensity</option>
        <option value="NumDS">Number of Dry Spells</option>
        <option value="NumWS">Number of Wet Spells</option></select>
      </fieldset>

      <fieldset class="navitem" id="yearlyStat">
        <legend>Yearly seasonal statistics</legend>
        <select class="pageformcopy" name="yearlyStat">
          <option value="Mean">Mean</option>
          <option value="StdDev">Standard deviation</option>
          <option value="PoE">Probability of exceeding</option></select>

          <span id="pET1"><input class="pageformcopy" name="probExcThresh1" type="text" value="40" size="3" maxlength="6"/>mm</span>
          <span id="pET2"><input class="pageformcopy" name="probExcThresh2" type="text" value="30" size="3" maxlength="3"/>days</span>
          <span id="pET3"><input class="pageformcopy" name="probExcThresh3" type="text" value="10" size="3" maxlength="3"/>mm/day</span>
          <span id="pET4"><input class="pageformcopy" name="probExcThresh4" type="text" value="3" size="3" maxlength="3"/>spells</span>
          <span id="pET5"><input class="pageformcopy" name="probExcThresh5" type="text" value="3" size="3" maxlength="3"/>spells</span>
        </fieldset>

        <fieldset class="navitem"><legend>Spatially Average Over</legend><span class="selectvalue"></span>
          <select class="pageformcopy" name="resolution">
            <option value="0.05">gridpoint</option>
            <option value="irids:SOURCES:Features:Political:World:secondOrder_GAUL:ds">District</option>
            <option value="irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds">Region</option>
          </select>

          <link class="admin" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/expert/(0.05)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A0%3A12%3A16%3A24%3Abb)//bbox/parameter/geoobject//resolution/get_parameter/geoobject/fullname/3/get//firstOrder_GAUL/eq/gid/exch/%7B779/786%7D%7B5835/5872%7Difelse/RANGE/geometryintersection/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(bb%3A8.5%3A16%3A9%3A16.5%3Abb)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json" />
          <select class="pageformcopy" name="region">
            <optgroup class="template" label="Label">
              <option class="iridl:values region@value label"></option>
            </optgroup>
          </select>

        </fieldset>
        <fieldset class="navitem"><legend>Wet/Dry Day definition</legend>Rainfall amount above/below<input class="pageformcopy" name="wetThreshold" type="text" value="1." size="5" maxlength="5"/>mm/day</fieldset>
        <fieldset class="navitem"><legend>Wet/Dry Spell definition</legend><input class="pageformcopy" name="spellThreshold" type="text" value="5" size="2" maxlength="2"/>continuous wet/dry days</fieldset>
      </div>

      <div class="ui-tabs">
        <ul class="ui-tabs-nav">
          <li><a href="#tabs-1" >Description</a></li>
          <li><a href="#tabs-2" >Dataset Documentation</a></li>
          <li><a href="#tabs-3" >Instructions</a></li>
          <li><a href="#tabs-4" >Contact Us</a></li>
        </ul>

        <fieldset class="regionwithinbbox dlimage bis" about="">
          <img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A-1%3A10%3A17%3A25%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(bb%3A3%3A14%3A3.5%3A14.5%3Abb)//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/SOURCES/.Features/.Political/.World/.firstOrder_GAUL_coarse/.the_geom/gid/779/786/RANGE/X/Y/fig-/lightgrey/mask/black/countries/red/fill/red/smallpushpins/black/thinnish/stroke/-fig//antialias/true/psdef//plotaxislength/200/psdef//plotborder/0/psdef/+.gif" /><br/><br/>

          <div class="valid" style="display: inline-block; text-align: top;">
            <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28bb:8:16:9:17:bb%29//region/parameter/geoobject/info.json"></a>
            <div class="template">Observations for <span class="bold iridl:long_name"></span></div>
          </div>
          <br /><br/>

          <img class="dlimgts" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/(1981)//YearStart/parameter/(last)//YearEnd/parameter/RANGEEDGES/(TotRain)//seasonalStat/parameter/(PerDA)/eq/%7Bdataflag%7Dif/(1)//DayStart/parameter/(%20)/append/(Jul)//seasonStart/parameter/append/(%20-%20)/append/(31)//DayEnd/parameter/append/(%20)/append/(Aug)//seasonEnd/parameter/append/(5)//spellThreshold/parameter/interp/(1)//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/(TotRain)/eq/%7Bnip/nip/flexseastotAvgFill//long_name/(Rainfall)/def//units/(mm)/def%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//long_name/(Count)/def//units/(unitless)/def%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//long_name/(Rainfall)/def//units/(mm/day)/def/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7BflexseasonalnonoverlapDSfreq//long_name/(Count)/def//units/(unitless)/def%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7BflexseasonalnonoverlapWSfreq//long_name/(Count)/def//units/(unitless)/def%7Dif//seasonalStat/get_parameter/(PerDA)/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/(bb%3A8.5%3A16%3A9%3A16.5%3Abb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/0.0/mul/T/fig-/grey/deltabars/-fig//plotaxislength/432/psdef//framelabel//seasonalStat/get_parameter/(TotRain)/eq/%7B/(Total%20Rainfall)/%7Dif//seasonalStat/get_parameter/(NumWD)/eq/%7B/(Number%20of%20Wet%20Days)/%7Dif//seasonalStat/get_parameter/(RainInt)/eq/%7B/(Rainfall%20Intensity)/%7Dif//seasonalStat/get_parameter/(NumDS)/eq/%7B/(Number%20of%20Dry%20Spells)/%7Dif//seasonalStat/get_parameter/(NumWS)/eq/%7B/(Number%20of%20Wet%20Spells)/%7Dif/(%20in%20season%20)/append/lpar/append//seasonStart/get_parameter/append/(%20)/append//DayStart/get_parameter/append/(%20-%20)/append//seasonEnd/get_parameter/append/(%20)/append//DayEnd/get_parameter/append/rpar/append/psdef//plotborderbottom/48/psdef//plotbordertop/45/psdef//fntsze/11.0/psdef+.gif" />
        </fieldset>

        <fieldset class="dlimage withMap" id="content" about="">
          <link rel="iridl:hasFigure" href="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/%281981%29//YearStart/parameter/%282019%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7Bdup/dataflag%5BT%5Dsum/dup/1.0/sub/div/sqrt/2/1/roll%5BT%5Drmsaover/mul/MEWSprcp_colors%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7BDATA/0/300/1/RANGESTEP%5BX/Y%5Ddistrib1D/dup/0.0%5Baprod%5Dintegrate/aprod/300.5/VALUE/aprod/removeGRID/div//name/%28pdf%29/def/DATA/null/null/RANGE/0.0%5Baprod%5Dintegrate//name/%28pdf%29/def/DATA/0/0.02/0.17/0.5/0.83/0.98/1.0001/VALUES/-1/mul/1/add/aprod//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28150%29//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%2830%29//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%2810%29//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%283%29//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%283%29//probExcThresh5/parameter/interp%7Dif/VALUE/%28percent%29/unitconvert//long_name/%28Probability%29/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap%7Dif//long_name/%28%0A/seasonalStat%20get_parameter%0A%28TotRain%29%20eq%0A%7B%20%28Total%20Rainfall%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWD%29%20eq%0A%7B%20%28Wet%20Days%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28RainInt%29%20eq%0A%7B%20%28Rainfall%20Intensity%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumDS%29%20eq%0A%7B%20%28Dry%20Spells%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWS%29%20eq%0A%7B%20%28Wet%20Spells%29%20%7Dif%0A%28%20:%20%29%0A/yearlyStat%20get_parameter%0A%28Mean%29%20eq%20%7B%20%28Average%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28StdDev%29%20eq%20%7B%20%28Standard%20Deviation%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28PoE%29%20eq%20%7B%20%28Probability%29%20%7Dif%0A%29/interp/3/array/astore/concat/def/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name/%28clim_var%29/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20for%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20to%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef/" />

          <img class="dlimg bis" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/%281981%29//YearStart/parameter/%282019%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7Bdup/dataflag%5BT%5Dsum/dup/1.0/sub/div/sqrt/2/1/roll%5BT%5Drmsaover/mul/MEWSprcp_colors%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7BDATA/0/300/1/RANGESTEP%5BX/Y%5Ddistrib1D/dup/0.0%5Baprod%5Dintegrate/aprod/300.5/VALUE/aprod/removeGRID/div//name/%28pdf%29/def/DATA/null/null/RANGE/0.0%5Baprod%5Dintegrate//name/%28pdf%29/def/DATA/0/0.02/0.17/0.5/0.83/0.98/1.0001/VALUES/-1/mul/1/add/aprod//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28150%29//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%2830%29//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%2810%29//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%283%29//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%283%29//probExcThresh5/parameter/interp%7Dif/VALUE/%28percent%29/unitconvert//long_name/%28Probability%29/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap%7Dif//long_name/%28%0A/seasonalStat%20get_parameter%0A%28TotRain%29%20eq%0A%7B%20%28Total%20Rainfall%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWD%29%20eq%0A%7B%20%28Wet%20Days%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28RainInt%29%20eq%0A%7B%20%28Rainfall%20Intensity%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumDS%29%20eq%0A%7B%20%28Dry%20Spells%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWS%29%20eq%0A%7B%20%28Wet%20Spells%29%20%7Dif%0A%28%20:%20%29%0A/yearlyStat%20get_parameter%0A%28Mean%29%20eq%20%7B%20%28Average%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28StdDev%29%20eq%20%7B%20%28Standard%20Deviation%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28PoE%29%20eq%20%7B%20%28Probability%29%20%7Dif%0A%29/interp/3/array/astore/concat/def/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name/%28clim_var%29/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20for%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20to%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef/+.gif" border="0"  alt="image" />

          <span id="auxvar">
            <img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/%281981%29//YearStart/parameter/%282019%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7Bdup/dataflag%5BT%5Dsum/dup/1.0/sub/div/sqrt/2/1/roll%5BT%5Drmsaover/mul/MEWSprcp_colors%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7BDATA/0/300/1/RANGESTEP%5BX/Y%5Ddistrib1D/dup/0.0%5Baprod%5Dintegrate/aprod/300.5/VALUE/aprod/removeGRID/div//name/%28pdf%29/def/DATA/null/null/RANGE/0.0%5Baprod%5Dintegrate//name/%28pdf%29/def/DATA/0/0.02/0.17/0.5/0.83/0.98/1.0001/VALUES/-1/mul/1/add/aprod//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28150%29//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%2830%29//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%2810%29//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%283%29//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%283%29//probExcThresh5/parameter/interp%7Dif/VALUE/%28percent%29/unitconvert//long_name/%28Probability%29/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap%7Dif//long_name/%28%0A/seasonalStat%20get_parameter%0A%28TotRain%29%20eq%0A%7B%20%28Total%20Rainfall%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWD%29%20eq%0A%7B%20%28Wet%20Days%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28RainInt%29%20eq%0A%7B%20%28Rainfall%20Intensity%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumDS%29%20eq%0A%7B%20%28Dry%20Spells%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWS%29%20eq%0A%7B%20%28Wet%20Spells%29%20%7Dif%0A%28%20:%20%29%0A/yearlyStat%20get_parameter%0A%28Mean%29%20eq%20%7B%20%28Average%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28StdDev%29%20eq%20%7B%20%28Standard%20Deviation%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28PoE%29%20eq%20%7B%20%28Probability%29%20%7Dif%0A%29/interp/3/array/astore/concat/def/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name/%28clim_var%29/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20for%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20to%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef/+.auxfig/+.gif" />
          </span>
          <span id="auxtopo">
            <img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/10/24/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif" />
          </span>

        </fieldset>
        <div id="tabs-1" class="ui-tabs-panel" about="">
          <h2 class="titre1" property="term:title">Daily Precipitation Analysis</h2>
          <p property="term:description">
            The Maproom explores historical daily precipitation by calculating simple seasonal statistics.</p>

            <p>Many options can be specified to produce yearly time series of a chosen seasonal diagnostic of the daily precipitation data. The user can then choose to map the mean, standard deviation or probability of exceeding a chosen threshold, over years; clicking on the map will then produce a local yearly time series of the chosen diagnostic.
            </p>
            <p align="left">
              <b>Years and Season</b>:
              Specify the range of years over which to perform the analysis and choose the start and end dates of the season , over which the diagnostics are to be performed.
              <br />
              <b>Wet/Dry Day/Spell Definitions</b>:
              These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
              <br /><br /><b>Seasonal daily statistics</b>: Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
              <br /><b>Total Rainfall</b>: total cumulative precipitation (in mm) falling over the season.
              <br /><b>Number of Wet Days</b>: the number of wet days (as defined above) during the season.
              <br /><b>Rainfall Intensity</b>:
              the average daily precipitation over the season considering only wet days.<br />
              <b>Number of Wet (Dry) Spells</b>:
              the number of wet (dry) spells during the season according to the definitions in the Options section. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
              <br /><br />
              <b>Yearly seasonal statistics</b>: a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
              <br />
              <br /><b>Spatial Resolution</b>:
              The analysis can performed and map at each 0.05˚ resolution grid point. Additionally it is possible to average the results of the analysis over the 0.05˚ grid points falling within administrative boundaries for the time series graph.
            </p>
          </div>

          <div id="tabs-2" class="ui-tabs-panel">
            <h2 class="titre1">Dataset Documentation</h2>
            <dl class="datasetdocumentation"> Reconstructed rainfall over land areas on a 0.05˚ x 0.05˚ lat/lon grid (about 5km). The rainfall <a href="/SOURCES/.Niger/">time series</a> (1981 to 2019) were created by combining quality-controlled station observations with satellite rainfall estimates. <br/> </dl>
          </div>

          <div id="tabs-3" class="ui-tabs-panel">
            <h2 class="titre1">How to use this interactive map</h2>
            <p style="padding-right:25px;">
              <div class="buttonInstructions bis"></div>
            </p>
          </div>

          <div id="tabs-4"  class="ui-tabs-panel">
            <h2 class="titre1">Helpdesk</h2>
            <p class="p1">Contact <a href="mailto:bernard.minoungou@cilss.int?subject= Daily Precipitation Analysis">bernard.minoungou@cilss.int</a> with any technical questions or problems with this Map Room.
            </p>
          </div>
        </div>
        <br/>

        <div class="optionsBar">
          <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
        </div>
      </body>
      </html>
