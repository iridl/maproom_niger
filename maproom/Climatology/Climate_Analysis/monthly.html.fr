<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="fr">

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <meta xml:lang="" property="maproom:Entry_Id" content="Climatology_Analysis" />
  <meta xml:lang="" property="maproom:Sort_Id" content="a3" />
  <title>Analyse Climatique Mensuelle</title>

  <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
  <link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
  <link rel="stylesheet" type="text/css" href="/maproom/niger.css" />

  <link class="share" rel="canonical" href="monthly.html" />
  <link class="carryLanguage" rel="home" href="http://iridl.ldeo.columbia.edu" />
  <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
  <link class="altLanguage" rel="alternate" hreflang="en" href="monthly.html?Set-Language=en" />

  <link rel="shortcut icon" href="/localconfig/icons/iriwh128.png" />
  <script type="text/javascript" src="/uicore/uicore.js"></script>
  <script type="text/javascript" src="/localconfig/ui.js"></script>
  <script type="text/javascript" src="/uicore/insertui.js"></script>
  <!-- <script type="text/javascript" src="/maproom/metmalawi.js"></script> -->

  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded" />

  <link rel="term:icon" href="/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/(.rfe_merged_chirps)//var/parameter/dup/interp/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/||/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef//antialias/true/psdef/T/last/plotvalue+.gif" />

  <style>
    body[layers] #auxvar {
      display: none;
    }
    body[layers] #auxtopo {
      display:none;
    }
    body[layers~="clim_var"] #auxvar {
      display: inline;
    }
    body[layers~="bath"] #auxtopo {
      display: inline;
    }
    body[layers~="clim_var"] #auxtopo {
      display: none;
    }
  </style>
</head>

<body xml:lang="fr">
  <form name="pageform" id="pageform" class="carryLanguage carryup carry dlimg dlauximg share">
    <input name="Set-Language" type="hidden" class="carryup carryLanguage"/>
    <input class="carry dlimg share dlimgloc dlimglocclick admin" name="bbox" type="hidden" />
    <input class="dlimg dlauximg share" name="T" type="hidden" data-default="Jan" />
    <input class="dlimg dlauximg" name="plotaxislength" type="hidden" />
    <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
    <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />

    <input class="carry share ver2" name="seasonStart" type="hidden" data-default="Apr" />
    <input class="carry share ver2" name="YearStart" type="hidden" data-default="1981" />
    <input class="carry share ver2" name="seasonEnd" type="hidden" data-default="Jun" />
    <input class="carry share ver2" name="YearEnd" type="hidden" data-default="2019" />

    <input class="carry share dlimg dlauximg dlimgts" name="var" type="hidden" data-default=".rfe_merged_chirps" />

    <input class="carry pickarea share admin" name="resolution" type="hidden" data-default="0.05" />
    <input class="dlimg share bodyAttribute" name="layers" value="clim_var" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="bath" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Regions" checked="checked" type="checkbox" />
    <input class="dlimg share bodyAttribute" name="layers" value="Districts" checked="checked" type="checkbox" />
  </form>

  <div class="controlBar">
    <fieldset class="navitem" id="toSectionList">
      <legend>Maproom</legend>
      <a rev="section" class="navlink carryup" href="/maproom/Climatology/">Climat</a>
    </fieldset>

    <fieldset class="navitem" id="chooseSection">
      <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis"><span property="term:label">Analyse Climatique</span></legend>
    </fieldset>

    <!-- <fieldset class="navitem">
      <legend>Region</legend>
      <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/metmalawiRegions.json"></a>
      <select class="RegionMenu" name="bbox">
        <option value="">Malawi</option>
        <optgroup class="template" label="Region">
          <option class="irigaz:hasPart irigaz:id@value term:label"></option>
        </optgroup>
      </select>
    </fieldset> -->

    <fieldset class="navitem"><legend>Variable</legend>
      <select class="pageformcopy" name="var">
        <option value=".rfe_merged_chirps">Précipitations</option>
        <option value=".tmin">Température Minimum</option>
        <option value=".tmax">Température Maximum</option>
        <!-- <option value=".tmean">Temperature Moyenne</option> -->
      </select>
    </fieldset>

    <fieldset class="navitem"><legend>Moyenne Spatiale</legend><span class="selectvalue"></span>
      <select class="pageformcopy" name="resolution">
        <option value="0.05">Point de grille</option>
        <option value="irids:SOURCES:Features:Political:World:secondOrder_GAUL:ds">District</option>
        <option value="irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds">Région</option>
      </select>

      <link class="admin" rel="iridl:hasJSON" href="http://iridl.ldeo.columbia.edu/expert/expert/(0.05)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A0%3A12%3A16%3A24%3Abb)//bbox/parameter/geoobject//resolution/get_parameter/geoobject/fullname/3/get//firstOrder_GAUL/eq/gid/exch/%7B779/786%7D%7B5835/5872%7Difelse/RANGE/geometryintersection/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(bb%3A8.5%3A16%3A9%3A16.5%3Abb)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json" />
      <select class="pageformcopy" name="region">
        <optgroup class="template" label="Label">
          <option class="iridl:values region@value label"></option>
        </optgroup>
      </select>

    </fieldset>

    <fieldset class="navitem"><legend>Anomalies Saisonnières</legend>
      <select class="pageformcopy" name="seasonStart">
        <option value="Jan">Janv</option>
        <option value="Feb">Fév</option>
        <option value="Mar">Mars</option>
        <option value="Apr">Avr</option>
        <option value="May">Mai</option>
        <option value="Jun">Juin</option>
        <option value="Jul">Juil</option>
        <option value="Aug">Août</option>
        <option value="Sep">Sept</option>
        <option value="Oct">Oct</option>
        <option value="Nov">Nov</option>
        <option value="Dec">Déc</option>
      </select> -

      <select class="pageformcopy" name="seasonEnd">
        <option value="Jan">Janv</option>
        <option value="Feb">Fév</option>
        <option value="Mar">Mars</option>
        <option value="Apr">Avr</option>
        <option value="May">Mai</option>
        <option value="Jun">Juin</option>
        <option value="Jul">Juil</option>
        <option value="Aug">Août</option>
        <option value="Sep">Sept</option>
        <option value="Oct">Oct</option>
        <option value="Nov">Nov</option>
        <option value="Dec">Déc</option>
      </select> &nbsp;

      <input class="pageformcopy" name="YearStart" type="text" value="1981" size="4" maxlength="4"/> à
      <input class="pageformcopy" name="YearEnd" type="text" value="2019" size="4" maxlength="4"/>
    </fieldset>
  </div>

  <div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
      <li><a href="#tabs-2" >Documentation des données</a></li>
      <li><a href="#tabs-3" >Instructions</a></li>
      <li><a href="#tabs-4" >Contact</a></li>
    </ul>

    <fieldset class="regionwithinbbox dlimage bis" about="">
      <img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A-1%3A10%3A17%3A25%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(bb%3A3%3A14%3A3.5%3A14.5%3Abb)//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/SOURCES/.Features/.Political/.World/.firstOrder_GAUL_coarse/.the_geom/gid/779/786/RANGE/X/Y/fig-/lightgrey/mask/black/countries/red/fill/red/smallpushpins/black/thinnish/stroke/-fig//antialias/true/psdef//plotaxislength/200/psdef//plotborder/0/psdef/+.gif" /> <br/><br/>

      <div align="center" class="valid change">
        <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28bb:8:16:9:17:bb%29//region/parameter/geoobject/info.json"></a>
        <div class="template">Observations pour <span class="bold iridl:long_name"></span></div>
      </div>

      <div align="center" id="notgridbox" class="valid change">
        <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28irids:SOURCES:Features:Political:World:secondOrder_GAUL:ds%29geoobject/%28bb:8:16:9:17:bb%29//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json"></a>
        <div class="template" align="center">situé près de <span class="bold iridl:value"></span></div>
      </div>
      <br /><br/><br/><br/>      <br /><br/><br/><br/>

      <!-- <div class="valid" style="display: inline-block; text-align: top;">
        <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
        <div class="template" align="center">Observations for <span class="bold iridl:long_name"></span></div>
      </div>
      <br /><br/><br/> -->

      <!-- <b>Monthly Precipitation Climatology</b>
      There are now overlays of the 5th, 50th and 95th percentiles.-->
      <img class="dlimgts bis" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.monthly/(.rfe_merged)//var/parameter/(.rfe_merged)/eq/%7B.rainfall/.CHIRPS%7D%7B.temperature%7Difelse//var/get_parameter/interp/DATA/null/null/RANGE/(bb%3A8.5%3A16%3A9%3A16.5%3Abb)//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/T/(1981)/(2010)/RANGE/T/12/splitstreamgrid/dup/%5BT2%5Daverage/exch/%5BT2%5D0.05/0.5/0.95/0/replacebypercentile/a%3A/percentile/0.05/VALUE/percentile/removeGRID//fullname/(5th%20%25-ile)/def/%3Aa%3A/percentile/0.5/VALUE/percentile/removeGRID//fullname/(50th%20%25-ile)/def/%3Aa%3A/percentile/0.95/VALUE/percentile/removeGRID//fullname/(95th%20%25-ile)/def//var/get_parameter/(.rfe_merged)/eq/%7BDATA/0/AUTO/RANGE%7Dif/%3Aa/2/index//var/get_parameter/(.rfe_merged)/eq/%7B0.0/mul%7D%7B%5BT%5Dminover%7Difelse/dup/6/-1/roll/exch/6/-2/roll/5/index/T/fig-/white/deltabars/grey/deltabars/solid/medium/green/line/blue/line/red/line/-fig//var/get_parameter/(.rfe_merged)/eq/%7B//framelabel/(%20Monthly%20Rainfall%20Climatology)/psdef%7Dif//var/get_parameter/(.tmax)/eq/%7B//framelabel/(%20Monthly%20Maximum%20Temperature%20Climatology)/psdef%7Dif//var/get_parameter/(.tmin)/eq/%7B//framelabel/(Monthly%20Minimum%20Temperature%20Climatology)/psdef%7Dif//plotbordertop/45/psdef//plotborderleft/60/psdef//plotborderbottom/45/psdef//plotaxislength/450/psdef//fntsze/12.0/psdef//antialias/true/def/+.gif" />
      <br/><br/><br/>

      <!-- <b>Yearly Seasonal Anomalies</b> -->
      <img class="dlimgts ver2" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.dekadal/(.rfe_merged)//var/parameter/(.rfe_merged)/eq/%7B.rainfall/.CHIRPS%7D%7B.temperature%7Difelse//var/get_parameter/(.rfe_merged)/eq/%7B/(.rfe_merged)/%7D%7B//var/get_parameter/(.tmax)/eq/%7B/(.tmax)/%7D%7B/(.tmin)/%7Difelse%7Difelse/interp/T/(Apr)//seasonStart/parameter/(-)/append/(Jun)//seasonEnd/parameter/append/(%20)/append/(1981)//YearStart/parameter/append/(-)/append/(2019)//YearEnd/parameter/append/0.0/seasonalAverage//var/get_parameter/(.rfe_merged)/eq/%7B//seasonEnd/get_parameter/interp//seasonStart/get_parameter/interp/sub/dup/0/ge/%7B1.0/add%7D%7B13.0/add%7Difelse/3.0/mul/mul%7Dif/dup/%5BT%5Daverage/sub/(bb%3A8.5%3A16%3A9%3A16.5%3Abb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average//var/get_parameter/(.rfe_merged)/eq/%7Bprcp_anomaly_max1000_colors2%7D%7Bsstacolorscale%7Difelse//name//ClimVar/def/DATA/null/null/RANGE/a%3A//var/get_parameter/(.rfe_merged)/eq/%7B//long_name/(Rainfall)/def%7Dif//var/get_parameter/(.tmax)/eq/%7B//long_name/(Maximum%20temperature)/def%7Dif//var/get_parameter/(.tmin)/eq/%7B//long_name/(Minimum%20temperature)/def%7Dif/%3Aa%3A/%3Aa/T/fig-/colorbars2/-fig//plotaxislength/400/psdef//var/get_parameter/(.rfe_merged)/eq/%7B//framelabel/(Yearly%20Seasonal%20Rainfall%20Anomalies%20%3A%20%20)//seasonStart/get_parameter/append/(-)/append//seasonEnd/get_parameter/append/psdef%7Dif//var/get_parameter/(.tmax)/eq/%7B//framelabel/(Yearly%20Seasonal%20Max%20Temperature%20Anomalies%20%3A%20%20)//seasonStart/get_parameter/append/(-)/append//seasonEnd/get_parameter/append/psdef%7Dif//var/get_parameter/(.tmin)/eq/%7B//framelabel/(Yearly%20Seasonal%20Min%20Temperature%20Anomalies%20%3A%20)//seasonStart/get_parameter/append/(-)/append//seasonEnd/get_parameter/append/psdef%7Dif//plotborderbottom/48/psdef//plotbordertop/45/psdef//fntsze/11.0/psdef/+.gif" />
    </fieldset>

    <fieldset class="dlimage withMap" id="content" about="">
      <link rel="iridl:hasFigure" href="/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/(.rfe_merged_chirps)//var/parameter/dup/interp/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef/" />

      <img class="dlimg bis" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/(.rfe_merged_chirps)//var/parameter/dup/interp/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef/T/last/plotvalue+.gif" border="0" alt="image" /><br />

      <span id="auxvar">
        <img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.Niger/.ENACTS/.monthly/.climatologies/(.rfe_merged_chirps)//var/parameter/dup/interp/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name//clim_var/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef/T/last/plotvalue+.auxfig/+.gif" />
      </span>

      <span id="auxtopo">
        <img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/10/24/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif" />
      </span>
    </fieldset>

    <div id="tabs-1" class="ui-tabs-panel" about="">
      <h2 class="titre1" property="term:title">Analyse Climatique Mensuelle</h2>
      <p property="term:description">Cette interface permet à l'utilisateur de visualiser les climatologies et anomalies de précipitations et température. Les climatologies sont définies pour la période de 1981 à 2019.
      </p>
    </div>

    <div id="tabs-2" class="ui-tabs-panel">
      <h2 class="titre1">Documentation des données</h2>
      <dl class="datasetdocumentation"> Séries temporelles de précipitations sur des grilles de 0.05˚ x 0.05˚ de latitute/longitude (environ 5km). Les <a href="/SOURCES/.Niger/">données</a> (de 1981 à 2019) sont produites en combinant des données in-situ de stations et des données de précipitations estimées par satellite, après contrôle de qualité. <br/> </dl>
    </div>

    <div id="tabs-3" class="ui-tabs-panel">
      <h2  class="titre1">Comment utiliser cette carte intéractive</h2>
      <p style="padding-right:25px;">
        <div class="buttonInstructions bis"></div>
      </p>
    </div>

    <div id="tabs-4"  class="ui-tabs-panel">
      <h2 class="titre1">Support</h2>
      <p class="p1">Contact <a href="mailto:bernard.minoungou@cilss.int?subject=Climate Analysis">bernard.minoungou@cilss.int</a> pour toute question technique ou problème avec cette maproom.
      </p>
    </div>
  </div>
  <br/>

  <div class="optionsBar">
    <fieldset class="navitem" id="share"><legend>Partager</legend></fieldset>
  </div>

</body>
</html>
