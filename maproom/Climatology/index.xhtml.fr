<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:wms="http://www.opengis.net/wms#"
xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
xmlns:xs="http://www.w3.org/2001/XMLSchema#"
version="XHTML+RDFa 1.0"
xml:lang="fr"
>

<head>
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />
  <meta property="maproom:Sort_Id" content="a02" />
  <title>Maproom Direction Nationale de la Météorologie</title>

  <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
  <link rel="stylesheet" type="text/css" href="/maproom/niger.css" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />

  <link class="carryLanguage" rel="home" href="http://iridl.ldeo.columbia.edu/" />
  <link rel="canonical" href="index.html" />
  <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />

  <link rel="shortcut icon" href="/localconfig/icons/iriwh128.png" /> 
  <link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />

  <link rel="term:icon" href="/SOURCES/.Niger/.ENACTS/.daily/.rainfall/.CHIRPS/.rfe_merged/T/%281981%29//YearStart/parameter/%282019%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7Bdataflag%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/0.0//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28Mean%29//yearlyStat/parameter/%28Mean%29/eq/%7B%5BT%5Daverage%7Dif//yearlyStat/get_parameter/%28StdDev%29/eq/%7Bdup/dataflag%5BT%5Dsum/dup/1.0/sub/div/sqrt/2/1/roll%5BT%5Drmsaover/mul/MEWSprcp_colors%7Dif//yearlyStat/get_parameter/%28PoE%29/eq/%7BDATA/0/300/1/RANGESTEP%5BX/Y%5Ddistrib1D/dup/0.0%5Baprod%5Dintegrate/aprod/300.5/VALUE/aprod/removeGRID/div//name/%28pdf%29/def/DATA/null/null/RANGE/0.0%5Baprod%5Dintegrate//name/%28pdf%29/def/DATA/0/0.02/0.17/0.5/0.83/0.98/1.0001/VALUES/-1/mul/1/add/aprod//seasonalStat/get_parameter/%28TotRain%29/eq/%7B/%28150%29//probExcThresh1/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B/%2830%29//probExcThresh2/parameter/interp%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B/%2810%29//probExcThresh3/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B/%283%29//probExcThresh4/parameter/interp%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B/%283%29//probExcThresh5/parameter/interp%7Dif/VALUE/%28percent%29/unitconvert//long_name/%28Probability%29/def/startcolormap/0.0/100.0/RANGE/white/ForestGreen/ForestGreen/0.0/VALUE/OliveDrab/1.0/VALUE/DarkKhaki/2.0/VALUE/khaki/5.0/VALUE/PaleGoldenrod/10.0/VALUE/yellow/20.0/VALUE/orange/30.0/VALUE/DarkOrange/40.0/VALUE/OrangeRed/50.0/VALUE/red/60.0/VALUE/DarkRed/80.0/VALUE/DarkFirebrick/100.0/VALUE/DarkBrown/endcolormap%7Dif//long_name/%28%0A/seasonalStat%20get_parameter%0A%28TotRain%29%20eq%0A%7B%20%28Total%20Rainfall%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWD%29%20eq%0A%7B%20%28Wet%20Days%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28RainInt%29%20eq%0A%7B%20%28Rainfall%20Intensity%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumDS%29%20eq%0A%7B%20%28Dry%20Spells%29%20%7Dif%0A/seasonalStat%20get_parameter%0A%28NumWS%29%20eq%0A%7B%20%28Wet%20Spells%29%20%7Dif%0A%28%20:%20%29%0A/yearlyStat%20get_parameter%0A%28Mean%29%20eq%20%7B%20%28Average%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28StdDev%29%20eq%20%7B%20%28Standard%20Deviation%29%20%7Dif%0A/yearlyStat%20get_parameter%0A%28PoE%29%20eq%20%7B%20%28Probability%29%20%7Dif%0A%29/interp/3/array/astore/concat/def/dup/SOURCES/.Features/.Political/.World/.Countries_GAUL/.the_geom/adm0_code/181/VALUE/rasterize/0/maskle/dataflag/0/maskle/mul//name/%28clim_var%29/def/SOURCES/.WORLDBATH/.bath/X/0/16/RANGE/Y/12/24/RANGE/1/index/SOURCES/.Features/.Political/.World/a:/.firstOrder_GAUL//name//Regions/def/gid/779/786/RANGE/.the_geom/:a:/.secondOrder_GAUL//name//Districts/def/gid/5835/5872/RANGE/.the_geom/:a/X/Y/fig-/colors/colors/||/colors/grey/thinnish/stroke/black/thin/stroke/-fig//layers%5B//clim_var//Districts//Regions%5Dpsdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20for%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20to%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef+//plotborder+0+psdef//plotaxislength+432+psdef//color_smoothing+1+psdef//antialias+true+psdef+.gif" />

  <script type="text/javascript" src="/uicore/uicore.js"></script>
  <!-- <script type="text/javascript" src="../../maproom/metmalawi.js"></script> -->

  <style>
    div {
      padding-left:10px;
      text-align: justify;
    }

    p {
      text-align: justify;
    }

    img {
      padding-right:5px;
      padding-bottom:5px;
    }
  </style>
</head>


<body xml:lang="fr">
  <form name="pageform" id="pageform">
    <input class="carryLanguage carryup" name="Set-Language" type="hidden" />
    <!-- <input class="titleLink itemImage" name="bbox" type="hidden" /> -->
  </form>

  <div class="controlBar">
    <fieldset class="navitem">
      <legend>DNM</legend>
      <a rev="section" class="navlink carryup" href="/maproom/">Maproom</a>
    </fieldset>

    <fieldset class="navitem">
      <legend>Maproom</legend>
      <span class="navtext">Climat</span>
    </fieldset>

    <!-- <fieldset class="navitem">
      <legend>Region</legend>
      <a class="carryLanguage" rel="iridl:hasJSON" href="../../maproom/metmalawiRegions.json"></a>
      <select class="RegionMenu" name="bbox">
        <option value="">Malawi</option>
        <optgroup class="template" label="Region">
          <option class="irigaz:hasPart irigaz:id@value term:label"></option>
        </optgroup>
      </select>
    </fieldset> -->
  </div>

  <div>
    <div id="content" class="searchDescription">
     <h2 property="term:title">Climat</h2>
     <p align="left" property="term:description">Conditions climatiques historiques, actuelles et prévisions pour le pays.
     </p>
   </div>

   <div class="rightcol tabbedentries" about="/maproom/Climatology/" >
    <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Analysis" />
    <!-- <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring" />
    <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Forecast" /> -->
  </div>
</div>

<div class="optionsBar">
  <fieldset class="navitem" id="share"><legend>Partager</legend>
  </fieldset>
</div>

</body>

</html>
